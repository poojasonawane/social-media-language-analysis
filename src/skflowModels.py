import pandas as pd
import tempfile
import tensorflow as tf
import skflow
from sklearn import metrics
from sklearn.cross_validation import KFold
import numpy as np
import scipy.stats as ss

data = pd.read_csv("rpca500_5k_ext.csv")

y = data['outcome_demog_gender']
X = pd.DataFrame(data)
X =X.drop(['group_id','outcome_demog_age','outcome_demog_gender','outcome_big5_ext'],1)
kf = KFold(len(X),5,shuffle = True)
X_matrix = ss.zscore(np.array(X))
Y_matrix = np.array(y)
max_acc = 0
for train_index,test_index in kf:
    #print("train_index: ",len(train_index))
    #print("test index : ",len(test_index))
    X_train = X_matrix[train_index]
    X_test = X_matrix[test_index]
    Y_train = Y_matrix[train_index]
    Y_test = Y_matrix[test_index]
    classifier = skflow.TensorFlowDNNClassifier(hidden_units=[3], n_classes=2,steps=1000, learning_rate=0.05)
    classifier.fit(X_train, Y_train)
    #skflow.TensorFlowClassifier.fit(X_train, Y_train,monitor=None, logdir=None)
    score = metrics.accuracy_score(Y_test, classifier.predict(X_test))
    #print(Y_test, classifier.predict(X_test))
    if(score > max_acc):
        max_acc = score
    #print("Accuracy: %f" % score)
    #print("---------------------------")
print "Accuracy: ",max_acc
#print(Y_test, classifier.predict(X_test))

def custom_model(X, y):
    layers = skflow.ops.dnn(X, [15,10], tf.tanh)
    return skflow.models.logistic_regression(layers, y)
tf_clf_c = skflow.TensorFlowEstimator(model_fn=custom_model, n_classes=2, batch_size=256, steps=100, learning_rate=0.01)
tf_clf_c.fit(X_train, Y_train)
metrics.accuracy_score(Y_test, tf_clf_c.predict(X_test))
print(metrics.accuracy_score(Y_test, tf_clf_c.predict(X_test)))
#print(Y_test, classifier.predict(X_test))

y = data['outcome_demog_age']
Y_matrix = np.array(y)
min_err = 0
for train_index,test_index in kf:
    #print("train_index: ",train_index)
    #print("test index : ",test_index)
    X_train = X_matrix[train_index]
    X_test = X_matrix[test_index]
    Y_train = Y_matrix[train_index]
    Y_test = Y_matrix[test_index]
    regressor = skflow.TensorFlowDNNRegressor(hidden_units=[2,2])
    regressor.fit(X_train, Y_train)
    score = metrics.r2_score(Y_test,regressor.predict(X_test))
    if score > min_err:
        min_err = score
    print"-----------------"
#print(Y_test,regressor.predict(X_test))
print("Score",score)

y = data['outcome_big5_ext']
Y_matrix = np.array(y)
min_err = 0
for train_index,test_index in kf:
    X_train = X_matrix[train_index]
    X_test = X_matrix[test_index]
    Y_train = Y_matrix[train_index]
    Y_test = Y_matrix[test_index]
    regressor = skflow.TensorFlowDNNRegressor(hidden_units=[2,2])
    regressor.fit(X_train, Y_train)
    score = metrics.r2_score(Y_test,regressor.predict(X_test))
    if score > min_err:
        min_err = score
    print"-----------------"
#print(Y_test,regressor.predict(X_test))
print("Score: ",score)