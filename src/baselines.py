import pandas as pd
from sklearn import metrics
from sklearn.cross_validation import KFold
import numpy as np
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Ridge
import scipy.stats as ss
data = pd.read_csv("C:/TempDrive/MS/Fall16/AdvProj/rpca500_5k_ext.csv")
X = pd.DataFrame(data)
X =X.drop(['group_id','outcome_demog_age','outcome_demog_gender', 'outcome_big5_ext'],1)
X_matrix = ss.zscore(np.array(X))
y = data['outcome_demog_gender']
kf = KFold(len(X),5,shuffle = True)
Y_matrix = np.array(y)
max_acc = 0
for train_index,test_index in kf:
    X_train = X_matrix[train_index]
    X_test = X_matrix[test_index]
    Y_train = Y_matrix[train_index]
    Y_test = Y_matrix[test_index]
    model = svm.LinearSVC(C=1) 
    #model = LogisticRegression()
    model.fit(X_train, Y_train)
    #skflow.TensorFlowClassifier.fit(X_train, Y_train,monitor=None, logdir=None)
    score = metrics.accuracy_score(Y_test, model.predict(X_test))
    if(score > max_acc):
        max_acc = score
print ("Accuracy: ",max_acc)

y = data['outcome_demog_age']
kf = KFold(len(X),5,shuffle = True)
Y_matrix = np.array(y)
min_scr = 0
for train_index,test_index in kf:
    X_train = X_matrix[train_index]
    X_test = X_matrix[test_index]
    Y_train = Y_matrix[train_index]
    Y_test = Y_matrix[test_index]
    model = Ridge(alpha=65) 
    model.fit(X_train, Y_train)
    score = metrics.r2_score(Y_test, model.predict(X_test))
    #score = metrics.mean_absolute_error(Y_test, model.predict(X_test))
    if(score > min_scr):
        min_scr = score
print ("Score: ",min_scr)

y = data['outcome_big5_ext']
kf = KFold(len(X),5,shuffle = True)
Y_matrix = np.array(y)
min_scr = 0
for train_index,test_index in kf:
    X_train = X_matrix[train_index]
    X_test = X_matrix[test_index]
    Y_train = Y_matrix[train_index]
    Y_test = Y_matrix[test_index]
    model = Ridge(alpha=100) 
    model.fit(X_train, Y_train)
    score = metrics.r2_score(Y_test, model.predict(X_test))
    if(score > min_scr):
        min_scr = score
print ("Score: ",min_scr)